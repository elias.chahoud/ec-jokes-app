package ec.springframework.ecjokesapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ec.springframework.ecjokesapp.service.JokesServiceImpl;

@Controller
public class JokesController {
  private final JokesServiceImpl jokesService;

  public JokesController(JokesServiceImpl jokesService) {
    this.jokesService = jokesService;
  }

  @GetMapping("/")
  public String tellJoke(Model model) {
    model.addAttribute("joke", jokesService.getJoke());
    return "index";
  }
}
