package ec.springframework.ecjokesapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ec.springframework.ecjokesapp.service.JokesService;

@SpringBootApplication
public class EcJokesAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcJokesAppApplication.class, args);

	}

}
