package ec.springframework.ecjokesapp.service;

public interface JokesService {

  String getJoke();

}
